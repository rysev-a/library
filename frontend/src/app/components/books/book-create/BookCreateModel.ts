import { action, makeObservable, observable, computed, toJS } from 'mobx';
import history from 'app/core/history';

import Form from 'app/core/plugins/Form';
import { bookApi } from 'app/services/books';
import { authorApi } from 'app/services/authors';
import { shelfApi } from 'app/services/shelves';

class BookCreateModel extends Form {
  constructor() {
    super();

    makeObservable(this, {
      authorOptions: observable,
      shelfOptions: observable,

      // actions
      loadAuthors: action,
      loadShelves: action,
    });
  }

  authorOptions = [];
  shelfOptions = [];

  loadAuthors = (query: any = null) => {
    authorApi.list
      .get({
        pagination: {
          offset: 0,
        },
        query,
      })
      .then(({ data }) => {
        this.authorOptions = data.results.map(this.serializeOptions);
      });
  };

  serializeOptions = ({ id, name }) => {
    return {
      value: id,
      label: name,
    };
  };

  setAuthors = (e) => {
    this.values['authors'] = e.map(({ value }) => ({ id: value }));
    this.resetFieldValidation('authors');
  };

  loadShelves = () => {
    shelfApi.list
      .get({
        pagination: {
          offset: 0,
        },
      })
      .then(({ data }) => {
        this.shelfOptions = data.results.map(this.serializeOptions);
      });
  };

  setShelves = (e) => {
    this.values['shelves'] = e.map(({ value }) => ({ id: value }));
    this.resetFieldValidation('shelves');
  };

  onInputAuthorChange = (name) => {
    this.loadAuthors({ name });
  };

  getDefaultValues = () => {
    return {
      title: '',
      description: '',
      authors: [],
    };
  };

  submit = (values) => {
    return bookApi.list.post(values);
  };

  onSuccessHandler = (response) => {
    history.push('/books');
  };

  onError = () => {
    this.processing = false;
  };

  serializeValues = (values) => {
    return { ...values, order: 0 };
  };
}

export default BookCreateModel;
