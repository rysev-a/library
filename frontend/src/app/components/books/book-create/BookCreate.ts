import BookCreateView from './BookCreateView';
import { inject, observer } from 'mobx-react';
import { InjectStore } from 'app/core/store';

export default inject(({ store }: InjectStore) => ({
  bookCreate: store.bookCreate,
}))(observer(BookCreateView));
