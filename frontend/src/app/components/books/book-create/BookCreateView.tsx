import * as React from 'react';
import ReactSelect from 'react-select';
import Processing from 'app/ui/Processing/Processing';
import FormControl, { ErrorMessage } from 'app/ui/FormControl/FormControl';
import BookCreateModel from './BookCreateModel';

interface InjectedProps {
  bookCreate: BookCreateModel;
}

class BookCreateView extends React.Component {
  get injected() {
    return this.props as InjectedProps;
  }

  componentWillUnmount() {
    this.injected.bookCreate.reset();
  }

  componentDidMount() {
    this.injected.bookCreate.loadAuthors();
    this.injected.bookCreate.loadShelves();
  }

  render() {
    const { bookCreate: form } = this.injected;

    return (
      <div className="create-project">
        <h1 className="is-size-3  title has-text-weight-normal title has-text-weight-normal">
          Добавить книгу в библиотеку
        </h1>
        <form className="signin-form" onSubmit={form.handleSubmit}>
          <Processing processing={form.processing} />
          <div className="columns">
            <div className="column">
              <div className="field">
                <label className="label">Название</label>
                <FormControl model={form} field="title" />
              </div>
              <div className="field">
                <label className="label">Описание</label>
                <FormControl
                  model={form}
                  field="description"
                  control="textarea"
                />
              </div>

              <div className="field">
                <div className="control form-control">
                  <label className="label">Авторы</label>
                  <ReactSelect
                    onInputChange={form.onInputAuthorChange}
                    isMulti
                    onChange={(e) => form.setAuthors(e)}
                    options={form.authorOptions}
                  />
                  <ErrorMessage error={form.errors['authors']} />
                </div>
              </div>
              <div className="field">
                <div className="control form-control">
                  <label className="label">Полки</label>
                  <ReactSelect
                    isMulti
                    onChange={(e) => form.setShelves(e)}
                    options={form.shelfOptions}
                  />
                  <ErrorMessage error={form.errors['shelves']} />
                </div>
              </div>
            </div>
          </div>
          <button
            className="button is-primary"
            type="submit"
            disabled={form.isDisabled}>
            Создать
          </button>
        </form>
      </div>
    );
  }
}

export default BookCreateView;
