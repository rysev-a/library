import './BookDetail.css';

import * as React from 'react';
import { Link } from 'react-router-dom';
import Processing from 'app/ui/Processing/Processing';
import BookDetailModel from './BookDetailModel';

interface InjectedProps {
  bookDetail: BookDetailModel;
  match: {
    params: {
      id: string;
    };
  };
}

class BookDetailView extends React.Component {
  get injected() {
    return this.props as InjectedProps;
  }

  componentWillUnmount() {
    this.injected.bookDetail.reset();
  }

  componentDidMount() {
    this.injected.bookDetail.setId(this.injected);
    this.injected.bookDetail.load();
  }

  render() {
    const { bookDetail } = this.injected;
    const { data, loaded, processing } = bookDetail;

    return (
      <div className="project">
        <Processing processing={processing} />

        <section className="hero is-small is-dark">
          <div className="hero-body">
            <div className="container">
              {loaded && (
                <div className="container">
                  <h1 className="is-size-3 title has-text-weight-normal">
                    {data.title}
                  </h1>
                </div>
              )}
            </div>
          </div>
        </section>
        <section>
          <div className="container">
            <div className="card mt-6">
              <div className="card-content">
                <div className="info-item">
                  <div className="info-item__title has-text-weight-semibol">
                    Описание
                  </div>
                  <div className="info-item__content">{data.description}</div>
                </div>

                <div className="info-item">
                  <div className="info-item__title has-text-weight-semibol">
                    Авторы
                  </div>
                  <div className="info-item__content tags is-medium">
                    {data.authors
                      ? data.authors.map((author) => {
                          return (
                            <span key={author.id} className="tag is-primary">
                              {author.name}
                            </span>
                          );
                        })
                      : null}
                  </div>
                </div>

                <div className="info-item">
                  <div className="info-item__title has-text-weight-semibol">
                    Полки
                  </div>
                  <div className="info-item__content tags is-medium">
                    {data.shelves
                      ? data.shelves.map((shelf) => {
                          return (
                            <span key={shelf.id} className="tag is-primary">
                              {shelf.name}
                            </span>
                          );
                        })
                      : null}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default BookDetailView;
