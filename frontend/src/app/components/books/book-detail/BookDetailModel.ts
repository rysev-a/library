import AsyncDetail from 'app/core/plugins/AsyncDetail';
import { bookApi } from 'app/services/books';

class BookDetailModel extends AsyncDetail {
  constructor() {
    super(bookApi);
  }

  defaultDetailData = () => {
    return {
      title: '',
      description: '',
      authors: [],
    };
  };
}

export default BookDetailModel;
