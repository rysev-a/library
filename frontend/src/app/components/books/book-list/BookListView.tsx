import * as React from 'react';
import { Link } from 'react-router-dom';
import Processing from 'app/ui/Processing/Processing';
import BookListModel from './BookListModel';
import Pagination from 'app/ui/Pagination/Pagination';

interface InjectedProps {
  bookList: BookListModel;
}

class ProjectListView extends React.Component {
  get injected() {
    return this.props as InjectedProps;
  }

  componentWillUnmount() {
    this.injected.bookList.reset();
  }

  componentDidMount() {
    this.injected.bookList.load();
  }

  render() {
    const { bookList } = this.injected;
    const { results, processing, loaded, remove } = bookList;

    return (
      <div className="books">
        <Processing processing={processing} />
        <h1 className="is-size-1  title has-text-weight-normal">Книги</h1>
        {loaded && results.length === 0 ? (
          <h1>Пока что нет ни одной книги</h1>
        ) : (
          <table className="table is-fullwidth">
            <thead>
              <tr>
                <th>Id</th>
                <th>Название</th>
                <td>Описание</td>
                <td>Действия</td>
              </tr>
            </thead>
            <tbody>
              {results.map(({ id, title, description }) => (
                <tr key={id}>
                  <th>{id}</th>
                  <td>
                    <Link to={`/books/${id}`}>{title}</Link>
                  </td>
                  <td>{description}</td>

                  <td>
                    <div className="is-grouped field">
                      <div className="control">
                        <a
                          className="button is-danger is-small"
                          onClick={() => remove(id)}>
                          Удалить
                        </a>
                      </div>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
            <tfoot>
              <tr>
                <td colSpan={4}>
                  <Pagination
                    page={bookList.page}
                    pages={bookList.pages}
                    setPage={bookList.setPage}
                  />
                </td>
              </tr>
            </tfoot>
          </table>
        )}
        <div className="buttons">
          <Link className="button is-success" to="/books/create">
            Создать книгу
          </Link>
        </div>
      </div>
    );
  }
}

export default ProjectListView;
