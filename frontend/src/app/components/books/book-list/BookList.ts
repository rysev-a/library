import BookListView from './BookListView';
import { inject, observer } from 'mobx-react';
import { InjectStore } from 'app/core/store';

export default inject(({ store }: InjectStore) => ({
  bookList: store.bookList,
}))(observer(BookListView));
