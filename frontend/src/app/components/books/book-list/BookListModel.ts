import AsyncList from 'app/core/plugins/AsyncList';
import { bookApi } from 'app/services/books';

class BookListModel extends AsyncList {
  constructor() {
    super(bookApi);
  }
}

export default BookListModel;
