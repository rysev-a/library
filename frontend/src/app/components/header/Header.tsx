import './Header.css';
import * as React from 'react';

import { Link } from 'react-router-dom';

const Header = () => {
  return (
    <header className="header">
      <div className="container">
        <nav className="navbar">
          <Link className="navbar-brand is-size-4" to="/">
            Library
          </Link>
          <div className="navbar-end account-menu">
            <Link className="navbar-item" to="/books">
              Книги
            </Link>

            <Link className="navbar-item" to="/authors">
              Авторы
            </Link>

            <Link className="navbar-item" to="/shelves">
              Полки
            </Link>
          </div>
        </nav>
      </div>
    </header>
  );
};

export default Header;
