import AuthorListView from './AuthorListView';
import { inject, observer } from 'mobx-react';
import { InjectStore } from 'app/core/store';

export default inject(({ store }: InjectStore) => ({
  authorList: store.authorList,
}))(observer(AuthorListView));
