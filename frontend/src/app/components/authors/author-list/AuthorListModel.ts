import AsyncList from 'app/core/plugins/AsyncList';
import { authorApi } from 'app/services/authors';

class AuthorListModel extends AsyncList {
  constructor() {
    super(authorApi);
  }
}

export default AuthorListModel;
