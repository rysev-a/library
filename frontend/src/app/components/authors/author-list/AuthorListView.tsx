import * as React from 'react';
import { Link } from 'react-router-dom';
import Processing from 'app/ui/Processing/Processing';
import AuthorListModel from './AuthorListModel';
import Pagination from 'app/ui/Pagination/Pagination';

interface InjectedProps {
  authorList: AuthorListModel;
}

class ProjectListView extends React.Component {
  get injected() {
    return this.props as InjectedProps;
  }

  componentWillUnmount() {
    this.injected.authorList.reset();
  }

  componentDidMount() {
    this.injected.authorList.load();
  }

  render() {
    const { authorList } = this.injected;
    const { results, processing, loaded, remove } = authorList;

    return (
      <div className="authors">
        <Processing processing={processing} />
        <h1 className="is-size-1  title has-text-weight-normal">Авторы</h1>
        {loaded && results.length === 0 ? (
          <h1>Пока что нет ни одного автора</h1>
        ) : (
          <table className="table is-fullwidth">
            <thead>
              <tr>
                <th>Id</th>
                <th>Имя автора</th>
                <th>Действия</th>
              </tr>
            </thead>
            <tbody>
              {results.map(({ id, name }) => (
                <tr key={id}>
                  <th>{id}</th>
                  <td>
                    <Link to={`/authors/${id}`}>{name}</Link>
                  </td>

                  <td>
                    <div className="is-grouped field">
                      <div className="control">
                        <a
                          className="button is-danger is-small"
                          onClick={() => remove(id)}>
                          Удалить
                        </a>
                      </div>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
            <tfoot>
              <tr>
                <td colSpan={4}>
                  <Pagination
                    page={authorList.page}
                    pages={authorList.pages}
                    setPage={authorList.setPage}
                  />
                </td>
              </tr>
            </tfoot>
          </table>
        )}
        <div className="buttons">
          <Link className="button is-success" to="/authors/create">
            Добавить автора
          </Link>
        </div>
      </div>
    );
  }
}

export default ProjectListView;
