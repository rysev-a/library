import history from 'app/core/history';

import Form from 'app/core/plugins/Form';
import { authorApi } from 'app/services/authors';

class BookCreateModel extends Form {
  getDefaultValues = () => {
    return {
      name: '',
    };
  };

  submit = (values) => {
    return authorApi.list.post(values);
  };

  onSuccessHandler = (response) => {
    history.push('/authors');
  };
}

export default BookCreateModel;
