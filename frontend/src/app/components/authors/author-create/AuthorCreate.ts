import AuthorCreateView from './AuthorCreateView';
import { inject, observer } from 'mobx-react';
import { InjectStore } from 'app/core/store';

export default inject(({ store }: InjectStore) => ({
  authorCreate: store.authorCreate,
}))(observer(AuthorCreateView));
