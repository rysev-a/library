import * as React from 'react';
import { Link } from 'react-router-dom';
import Processing from 'app/ui/Processing/Processing';
import AuthorDetailModel from './AuthorDetailModel';

interface InjectedProps {
  authorDetail: AuthorDetailModel;
  match: {
    params: {
      id: string;
    };
  };
}

class AuthorDetailView extends React.Component {
  get injected() {
    return this.props as InjectedProps;
  }

  componentWillUnmount() {
    this.injected.authorDetail.reset();
  }

  componentDidMount() {
    this.injected.authorDetail.setId(this.injected);
    this.injected.authorDetail.load().then(
      this.injected.authorDetail.loadBooks({
        author_id: this.injected.authorDetail.id,
      })
    );
  }

  render() {
    const { authorDetail } = this.injected;
    const { data, loaded, processing, books } = authorDetail;

    return (
      <div className="author">
        <Processing processing={processing} />

        <section className="hero is-small is-dark">
          <div className="hero-body">
            <div className="container">
              {loaded && (
                <div className="container">
                  <h1 className="is-size-3 title has-text-weight-normal">
                    {data.name}
                  </h1>
                </div>
              )}
            </div>
          </div>
        </section>
        <section>
          <div className="container mt-5">
            <h2 className="title is-size-3 has-text-weight-normal">
              Книги автора
            </h2>
            {books.loaded && books.results.length === 0 ? (
              <h1>Пока что нет ни одной книги</h1>
            ) : (
              <table className="table is-fullwidth">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Название</th>
                    <td>Описание</td>
                  </tr>
                </thead>
                <tbody>
                  {books.results.map(({ id, title, description }) => (
                    <tr key={id}>
                      <th>{id}</th>
                      <td>
                        <Link to={`/books/${id}`}>{title}</Link>
                      </td>
                      <td>{description}</td>
                    </tr>
                  ))}
                </tbody>
              </table>
            )}
          </div>
        </section>
      </div>
    );
  }
}

export default AuthorDetailView;
