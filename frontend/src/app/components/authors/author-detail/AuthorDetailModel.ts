import AsyncDetail from 'app/core/plugins/AsyncDetail';
import { authorApi } from 'app/services/authors';
import { bookApi } from 'app/services/books';
import { action, makeObservable, observable } from 'mobx';

class AuthorDetailModel extends AsyncDetail {
  constructor() {
    super(authorApi);

    makeObservable(this, {
      books: observable,
      loadBooks: action,
    });
  }

  defaultDetailData = () => {
    return {
      name: '',
    };
  };

  books = {
    loaded: false,
    results: [],
  };

  loadBooks = (query: any = null) => {
    bookApi.list
      .get({
        pagination: {
          offset: 0,
        },
        query,
      })
      .then(({ data }) => {
        this.books = {
          loaded: true,
          results: data.results,
        };
      });
  };
}

export default AuthorDetailModel;
