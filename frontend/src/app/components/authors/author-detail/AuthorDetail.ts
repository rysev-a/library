import AuthorDetailView from './AuthorDetailView';
import { inject, observer } from 'mobx-react';
import { InjectStore } from 'app/core/store';

import { withRouter } from 'react-router-dom';

export default withRouter(
  inject(({ store }: InjectStore) => ({
    authorDetail: store.authorDetail,
  }))(observer(AuthorDetailView))
);
