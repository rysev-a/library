import ShelfCreateView from './ShelfCreateView';
import { inject, observer } from 'mobx-react';
import { InjectStore } from 'app/core/store';

export default inject(({ store }: InjectStore) => ({
  shelfCreate: store.shelfCreate,
}))(observer(ShelfCreateView));
