import * as React from 'react';
import Processing from 'app/ui/Processing/Processing';
import FormControl from 'app/ui/FormControl/FormControl';
import ShelfCreateModel from './ShelfCreateModel';

interface InjectedProps {
  shelfCreate: ShelfCreateModel;
}

class ShelfCreateView extends React.Component {
  get injected() {
    return this.props as InjectedProps;
  }

  componentWillUnmount() {
    this.injected.shelfCreate.reset();
  }

  render() {
    const { shelfCreate: form } = this.injected;

    return (
      <div className="create-shelf">
        <h1 className="is-size-3  title has-text-weight-normal title has-text-weight-normal">
          Добавить полку в библиотеку
        </h1>
        <form className="signin-form" onSubmit={form.handleSubmit}>
          <Processing processing={form.processing} />
          <div className="columns">
            <div className="column">
              <div className="field">
                <label className="label">Имя полки</label>
                <FormControl model={form} field="name" />
              </div>
            </div>
          </div>
          <button
            className="button is-primary"
            type="submit"
            disabled={form.isDisabled}>
            Создать
          </button>
        </form>
      </div>
    );
  }
}

export default ShelfCreateView;
