import history from 'app/core/history';

import Form from 'app/core/plugins/Form';
import { shelfApi } from 'app/services/shelves';

class ShelfCreateModel extends Form {
  getDefaultValues = () => {
    return {
      name: '',
    };
  };

  submit = (values) => {
    return shelfApi.list.post(values);
  };

  onSuccessHandler = (response) => {
    history.push('/shelves');
  };
}

export default ShelfCreateModel;
