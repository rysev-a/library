import * as React from 'react';
import { Link } from 'react-router-dom';
import Processing from 'app/ui/Processing/Processing';
import ShelfListModel from './ShelfListModel';
import Pagination from 'app/ui/Pagination/Pagination';

interface InjectedProps {
  shelfList: ShelfListModel;
}

class ProjectListView extends React.Component {
  get injected() {
    return this.props as InjectedProps;
  }

  componentWillUnmount() {
    this.injected.shelfList.reset();
  }

  componentDidMount() {
    this.injected.shelfList.load();
  }

  render() {
    const { shelfList } = this.injected;
    const { results, processing, loaded, remove } = shelfList;

    return (
      <div className="shelves">
        <Processing processing={processing} />
        <h1 className="is-size-1  title has-text-weight-normal">Полки</h1>
        {loaded && results.length === 0 ? (
          <h1>Пока что нет ни одной полки</h1>
        ) : (
          <table className="table is-fullwidth">
            <thead>
              <tr>
                <th>Id</th>
                <th>Имя полки</th>
                <th>Действия</th>
              </tr>
            </thead>
            <tbody>
              {results.map(({ id, name }) => (
                <tr key={id}>
                  <th>{id}</th>
                  <td>
                    <Link to={`/shelves/${id}`}>{name}</Link>
                  </td>

                  <td>
                    <div className="is-grouped field">
                      <div className="control">
                        <a
                          className="button is-danger is-small"
                          onClick={() => remove(id)}>
                          Удалить
                        </a>
                      </div>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
            <tfoot>
              <tr>
                <td colSpan={4}>
                  <Pagination
                    page={shelfList.page}
                    pages={shelfList.pages}
                    setPage={shelfList.setPage}
                  />
                </td>
              </tr>
            </tfoot>
          </table>
        )}
        <div className="buttons">
          <Link className="button is-success" to="/shelves/create">
            Добавить полку
          </Link>
        </div>
      </div>
    );
  }
}

export default ProjectListView;
