import ShelfListView from './ShelfListView';
import { inject, observer } from 'mobx-react';
import { InjectStore } from 'app/core/store';

export default inject(({ store }: InjectStore) => ({
  shelfList: store.shelfList,
}))(observer(ShelfListView));
