import AsyncList from 'app/core/plugins/AsyncList';
import { shelfApi } from 'app/services/shelves';

class ShelfListModel extends AsyncList {
  constructor() {
    super(shelfApi);
  }
}

export default ShelfListModel;
