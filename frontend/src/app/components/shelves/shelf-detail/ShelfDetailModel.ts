import AsyncDetail from 'app/core/plugins/AsyncDetail';
import { shelfApi } from 'app/services/shelves';
import { bookApi } from 'app/services/books';
import { action, makeObservable, observable } from 'mobx';

const defaultBooksState = () => {
  return {
    loaded: false,
    results: [],
    offset: 0,
    query: '',
    more: true,
  };
};

class ShelfDetailModel extends AsyncDetail {
  constructor() {
    super(shelfApi);

    makeObservable(this, {
      books: observable,
      loadBooks: action,
    });
  }

  defaultDetailData = () => {
    return {
      name: '',
    };
  };

  books: any = defaultBooksState();

  loadMore = () => {
    const nextOffset = this.books.offset + 10;
    this.loadBooks(nextOffset, this.books.query);
  };

  resetBooks = () => {
    this.books = defaultBooksState();
  };

  onSortEnd = ({ newIndex, oldIndex }) => {
    const max = newIndex > oldIndex ? newIndex : oldIndex;
    const min = newIndex < oldIndex ? newIndex : oldIndex;

    const updates = this.books.results.map((book, idx) => {
      if (idx < min) {
        return book;
      }

      if (idx > max) {
        return book;
      }

      if (newIndex === min) {
        if (idx === newIndex) {
          return {
            ...this.books.results[oldIndex],
            order: this.books.results[newIndex].order,
          };
        }

        return {
          ...this.books.results[idx - 1],
          order: this.books.results[idx - 1].order + 1,
        };
      }

      if (newIndex === max) {
        if (idx === newIndex) {
          return {
            ...this.books.results[oldIndex],
            order: this.books.results[newIndex].order,
          };
        }

        return {
          ...this.books.results[idx + 1],
          order: this.books.results[idx + 1].order - 1,
        };
      }

      return book;
    });

    this.books.results = updates;

    Promise.all(
      updates.slice(min, max + 1).map((book) => {
        return bookApi.detail.put(book.id, book);
      })
    );
  };

  loadBooks = (offset = 0, query: any = null) => {
    bookApi.list
      .get({
        pagination: {
          offset,
        },
        query,
      })
      .then(({ data }) => {
        console.log(data);
        const nextResults = [...this.books.results, ...data.results];

        this.books = {
          loaded: true,
          results: nextResults,
          offset,
          query,
          more: offset + 10 < data.count,
        };
      });
  };
}

export default ShelfDetailModel;
