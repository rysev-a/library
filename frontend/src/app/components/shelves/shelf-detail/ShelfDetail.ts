import ShelfDetailView from './ShelfDetailView';
import { inject, observer } from 'mobx-react';
import { InjectStore } from 'app/core/store';

import { withRouter } from 'react-router-dom';

export default withRouter(
  inject(({ store }: InjectStore) => ({
    shelfDetail: store.shelfDetail,
  }))(observer(ShelfDetailView))
);
