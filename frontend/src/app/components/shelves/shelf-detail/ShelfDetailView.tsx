import * as React from 'react';
import { Link } from 'react-router-dom';
import Processing from 'app/ui/Processing/Processing';
import ShelfDetailModel from './ShelfDetailModel';

import { SortableContainer, SortableElement } from 'react-sortable-hoc';

const SortableItem = SortableElement(({ id, title, description, order }) => (
  <tr key={id}>
    <th>{id}</th>
    <td>
      <Link to={`/books/${id}`}>{title}</Link>
    </td>
    <td>{description}</td>
    <td>{order}</td>
  </tr>
));

const SortableList = SortableContainer(({ items }) => {
  return (
    <tbody>
      {items.map((item, index) => {
        return (
          <SortableItem
            key={`item-${item.id}`}
            index={index}
            id={item.id}
            title={item.title}
            description={item.description}
            order={item.order}
          />
        );
      })}
    </tbody>
  );
});

interface InjectedProps {
  shelfDetail: ShelfDetailModel;
  match: {
    params: {
      id: string;
    };
  };
}

class ShelfDetailView extends React.Component {
  get injected() {
    return this.props as InjectedProps;
  }

  componentWillUnmount() {
    this.injected.shelfDetail.reset();
    this.injected.shelfDetail.resetBooks();
  }

  componentDidMount() {
    this.injected.shelfDetail.setId(this.injected);
    this.injected.shelfDetail.load().then(() => {
      this.injected.shelfDetail.loadBooks(0, {
        shelf_id: this.injected.shelfDetail.id,
      });
    });
  }

  render() {
    const { shelfDetail } = this.injected;
    const {
      data,
      loaded,
      processing,
      books,
      loadMore,
      onSortEnd,
    } = shelfDetail;

    return (
      <div className="shelf">
        <Processing processing={processing} />
        <section className="hero is-small is-dark">
          <div className="hero-body">
            <div className="container">
              {loaded && (
                <div className="container">
                  <h1 className="is-size-3 title has-text-weight-normal">
                    {data.name}
                  </h1>
                </div>
              )}
            </div>
          </div>
        </section>
        <section>
          <div className="container mt-5">
            <h2 className="title is-size-3 has-text-weight-normal">
              Книги на полке
            </h2>
            {books.loaded && books.results.length === 0 ? (
              <h1>Пока что нет ни одной книги</h1>
            ) : (
              <table className="table is-fullwidth">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Название</th>
                    <td>Описание</td>
                    <td>Порядок</td>
                  </tr>
                </thead>
                <SortableList onSortEnd={onSortEnd} items={books.results} />
              </table>
            )}
            {books.more && (
              <a className="is-success button" onClick={loadMore}>
                Загрузить ещё
              </a>
            )}
          </div>
        </section>
      </div>
    );
  }
}

export default ShelfDetailView;
