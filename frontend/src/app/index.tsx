import 'bulma/css/bulma.min.css';
import 'normalize.css/normalize.css';
import '@fortawesome/fontawesome-free/css/all.css';

import * as React from 'react';
import { render } from 'react-dom';
import history from 'app/core/history';
import store from 'app/core/store';
import { Provider } from 'mobx-react';
import { Router, Switch, Route } from 'react-router-dom';

import Header from './components/header/Header';
import StartPage from './pages/StartPage';

import BookListPage from './pages/books/BookListPage';
import BookCreatePage from './pages/books/BookCreatePage';
import BookDetailPage from './pages/books/BookDetailPage';

import AuthorListPage from './pages/authors/AuthorListPage';
import AuthorCreatePage from './pages/authors/AuthorCreatePage';
import AuthorDetailPage from './pages/authors/AuthorDetailPage';

import ShelfListPage from './pages/shelves/ShelfListPage';
import ShelfCreatePage from './pages/shelves/ShelfCreatePage';
import ShelfDetailPage from './pages/shelves/ShelfDetailPage';

const App = () => {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Header />
        <Switch>
          <Route path="/authors/create">
            <AuthorCreatePage />
          </Route>
          <Route path="/authors/:id">
            <AuthorDetailPage />
          </Route>
          <Route path="/authors">
            <AuthorListPage />
          </Route>
          <Route path="/books/create">
            <BookCreatePage />
          </Route>
          <Route path="/books/:id">
            <BookDetailPage />
          </Route>
          <Route path="/books">
            <BookListPage />
          </Route>
          <Route path="/shelves/create">
            <ShelfCreatePage />
          </Route>
          <Route path="/shelves/:id">
            <ShelfDetailPage />
          </Route>
          <Route path="/shelves">
            <ShelfListPage />
          </Route>
          <Route path="/">
            <StartPage />
          </Route>
        </Switch>
      </Router>
    </Provider>
  );
};

const container = document.getElementById('app');
render(<App />, container);
