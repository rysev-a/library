import axios from 'app/core/axios';
export const shelfApi = {
  list: {
    get: ({ pagination }) => {
      return axios.get(`/api/shelves/?offset=${pagination.offset}`);
    },
    post: (shelf) => {
      return axios.post(`/api/shelves/`, shelf);
    },
  },
  detail: {
    delete: (id) => {
      return axios.delete(`/api/shelves/${id}`);
    },
    get: (id) => {
      return axios.get(`/api/shelves/${id}`);
    },
  },
};
