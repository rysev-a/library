import axios from 'app/core/axios';
import { getQueryParams } from './utils';

export const authorApi = {
  list: {
    get: ({ pagination, query = null }) => {
      const params = getQueryParams(query);
      return axios.get(`/api/authors/?offset=${pagination.offset}${params}`);
    },
    post: (author) => {
      return axios.post(`/api/authors/`, author);
    },
  },
  detail: {
    delete: (id) => {
      return axios.delete(`/api/authors/${id}`);
    },
    get: (id) => {
      return axios.get(`/api/authors/${id}`);
    },
  },

  loadBooks: (authorId) => {
    return axios.get(`/api/filter-books/?author=${authorId}`);
  },
};
