import axios from 'app/core/axios';
import { getQueryParams } from './utils';

export const bookApi = {
  list: {
    get: ({ pagination, query = null }) => {
      const params = getQueryParams(query);
      return axios.get(`/api/books/?offset=${pagination.offset}${params}`);
    },
    post: (book) => {
      return axios.post(`/api/books/`, book);
    },
  },
  detail: {
    delete: (id) => {
      return axios.delete(`/api/books/${id}`);
    },
    get: (id) => {
      return axios.get(`/api/books/${id}`);
    },
    put: (id, payload) => {
      return axios.put(`/api/books/${id}/`, payload);
    },
  },
};
