export const getQueryParams = (query) => {
  if (query) {
    return (
      '&' +
      Object.keys(query)
        .map((paramKey) => {
          return `${paramKey}=${query[paramKey]}`;
        })
        .join('&')
    );
  }

  return '';
};
