import * as React from 'react';
import BookList from 'app/components/books/book-list/BookList';

const BookListPage = () => (
  <div className="section">
    <div className="container">
      <BookList />
    </div>
  </div>
);

export default BookListPage;
