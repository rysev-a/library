import * as React from 'react';
import BookDetail from 'app/components/books/book-detail/BookDetail';

const BookDetailPage = () => (
  <div className="section">
    <div className="container">
      <BookDetail />
    </div>
  </div>
);

export default BookDetailPage;
