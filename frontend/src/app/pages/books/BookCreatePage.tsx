import * as React from 'react';
import BookCreate from 'app/components/books/book-create/BookCreate';

const BookCreatePage = () => (
  <div className="section">
    <div className="container">
      <BookCreate />
    </div>
  </div>
);

export default BookCreatePage;
