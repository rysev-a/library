import * as React from 'react';
import AuthorList from 'app/components/authors/author-list/AuthorList';

const AuthorListPage = () => (
  <div className="section">
    <div className="container">
      <AuthorList />
    </div>
  </div>
);

export default AuthorListPage;
