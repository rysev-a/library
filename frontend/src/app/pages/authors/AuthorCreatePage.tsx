import * as React from 'react';
import AuthorCreate from 'app/components/authors/author-create/AuthorCreate';

const AuthorCreatePage = () => (
  <div className="section">
    <div className="container">
      <AuthorCreate />
    </div>
  </div>
);

export default AuthorCreatePage;
