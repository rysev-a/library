import * as React from 'react';
import AuthorDetail from 'app/components/authors/author-detail/AuthorDetail';

const AuthorDetailPage = () => (
  <div className="section">
    <div className="container">
      <AuthorDetail />
    </div>
  </div>
);

export default AuthorDetailPage;
