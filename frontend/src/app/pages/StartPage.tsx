import * as React from 'react';

const StartPage = () => (
  <div className="section">
    <div className="container">Добро пожаловать в библиотеку</div>
  </div>
);

export default StartPage;
