import * as React from 'react';
import ShelfList from 'app/components/shelves/shelf-list/ShelfList';

const ShelfListPage = () => (
  <div className="section">
    <div className="container">
      <ShelfList />
    </div>
  </div>
);

export default ShelfListPage;
