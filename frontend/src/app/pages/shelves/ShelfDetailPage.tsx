import * as React from 'react';
import ShelfDetail from 'app/components/shelves/shelf-detail/ShelfDetail';

const ShelfDetailPage = () => (
  <div className="section">
    <div className="container">
      <ShelfDetail />
    </div>
  </div>
);

export default ShelfDetailPage;
