import * as React from 'react';
import ShelfCreate from 'app/components/shelves/shelf-create/ShelfCreate';

const ShelfCreatePage = () => (
  <div className="section">
    <div className="container">
      <ShelfCreate />
    </div>
  </div>
);

export default ShelfCreatePage;
