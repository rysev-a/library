import './FormControl.css';
import * as React from 'react';
import { observer } from 'mobx-react';
import classNames from 'classnames';
import translate from 'app/core/utils/translate';

export const ErrorMessage = ({ error }) => {
  if (!error) {
    return null;
  }

  if (Array.isArray(error)) {
    return (
      <div>
        {error.map((errorMessage) => {
          return <ErrorMessage key={errorMessage} error={errorMessage} />;
        })}
      </div>
    );
  }

  return (
    <p className="help is-danger form-control-error">
      {translate({ text: error })}
    </p>
  );
};

const InputControl = observer(
  ({ field, type = 'text', model, placeholder = '' }) => {
    return (
      <div className="control form-control">
        <input
          className={classNames('input', {
            'is-danger': model.errors[field],
          })}
          type={type}
          name={field}
          onKeyUp={model.handleChange}
          onBlur={model.handleBlur}
          defaultValue={model.values[field]}
          placeholder={placeholder}
        />
        <ErrorMessage error={model.errors[field]} />
      </div>
    );
  }
);

const TextAreaControl = observer(({ field, model, placeholder = '' }) => (
  <div className="control form-control">
    <textarea
      className={classNames('textarea', {
        'is-danger': model.errors[field],
      })}
      name={field}
      onKeyUp={model.handleChange}
      onBlur={model.handleBlur}
      defaultValue={model.values[field]}
      placeholder={placeholder}
    />
    <ErrorMessage error={model.errors[field]} />
  </div>
));

const controls = {
  input: InputControl,
  textarea: TextAreaControl,
};

const FormControl = ({
  field,
  model,
  type = 'text',
  control = 'input',
  placeholder = '',
}) => {
  const Control = controls[control];
  return (
    <Control
      model={model}
      field={field}
      type={type}
      placeholder={placeholder}
    />
  );
};

export default FormControl;
