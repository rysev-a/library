import { action, makeObservable, observable, computed } from 'mobx';

interface FormModel {
  // callBacks
  handleChange(e): void;
  handleBlur(e): void;
  validateField(field): void;
  validateForm(): void;
  resetFieldValidation(field): void;

  // data
  processing: boolean;
  values: {};
  errors: {};
}

class Form implements FormModel {
  constructor() {
    makeObservable(this, {
      processing: observable,
      errors: observable,
      values: observable,

      // actions
      handleBlur: action,
      handleChange: action,
      handleFocus: action,
      resetFieldValidation: action,
      validateField: action,
      validateForm: action,
      handleSubmit: action,
      onError: action,
      onSuccess: action,
    });
  }

  processing = false;
  errors = {};
  values = {};

  handleChange = (e) => {
    this.values[e.target.name] = e.target.value;
    this.resetFieldValidation(e.target.name);
  };

  handleBlur = (e) => {
    const fieldName = e.target.name;
    this.validateField(fieldName);
  };

  handleFocus = (e) => {
    const fieldName = e.target.name;
    this.errors = {
      ...this.errors,
      [fieldName]: null,
    };
  };

  resetFieldValidation = (field) => {
    this.errors = {
      ...this.errors,
      [field]: '',
    };
  };

  validateField = (field) => {
    const value = this.values[field];
    this.errors = {
      ...this.errors,
      [`${field}`]: this.validate({ name: field, value }) || '',
    };
  };

  validateForm = () => {
    Object.keys(this.values).map((field) => this.validateField(field));
  };

  @computed
  get isDisabled() {
    return Object.keys(this.errors).some((key: any) => this.errors[key]);
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.validateForm();

    if (this.isDisabled) {
      return false;
    }

    this.processing = true;
    this.submit(this.serializeValues(this.values))
      .then((response) => {
        this.onSuccess(response);
      })
      .catch(({ response }) => {
        this.onError(response);
      });
  };

  onError = (response) => {
    this.processing = false;

    const { data } = response;

    this.errors = {
      ...this.errors,
      ...data,
    };
  };

  onSuccess = (response) => {
    this.processing = false;
    this.onSuccessHandler(this.values, response);
  };

  reset = () => {
    this.values = this.getDefaultValues();
    this.errors = {};
  };

  // implement in child
  submit = (values) => Promise.resolve(values);
  onSuccessHandler = (values, response) => console.log(values, response);
  serializeValues = (values) => values;
  getDefaultValues = () => ({});
  validate = (values) => '';
}

export default Form;
