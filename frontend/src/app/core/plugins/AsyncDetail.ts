import { action, makeObservable, observable } from 'mobx';

interface ServerListApi {
  list: {
    get: any;
    post: any;
  };
  detail: {
    get: any;
    delete: any;
  };
}

class AsyncDetail {
  api: ServerListApi;
  constructor(api) {
    this.api = api;
    this.data = this.defaultDetailData();

    makeObservable(this, {
      processing: observable,
      loaded: observable,
      data: observable,

      // actions
      setId: action,
      load: action,
      onSuccess: action,
      reset: action,
    });
  }

  remove = (id) => {
    this.api.detail.delete(id).then(() => {
      this.load();
    });
  };

  data: any;
  loaded = false;
  processing = false;
  id = 0;

  setId = ({
    match: {
      params: { id },
    },
  }) => {
    this.id = id;
  };

  load = () => {
    this.processing = true;
    return this.api.detail
      .get(this.id)
      .then(this.onSuccess)
      .catch(() => {
        this.processing = false;
      });
  };

  onSuccess = ({ data }) => {
    this.data = data;
    this.loaded = true;
    this.processing = false;
  };

  reset = () => {
    this.data = this.defaultDetailData();
    this.loaded = false;
  };

  defaultDetailData = () => ({});
}

export default AsyncDetail;
