import { action, makeObservable, observable } from 'mobx';

const ITEMS_ON_PAGE = 10;

const defaultPagination = () => ({
  count: 0,
  offset: 0,
});

interface ServerListApi {
  list: {
    get: any;
    post: any;
  };
  detail: {
    get: any;
    delete: any;
  };
}

class AsyncList {
  api: ServerListApi;
  processing = false;
  loaded = false;
  results = [];
  pagination = defaultPagination();

  constructor(api) {
    this.api = api;
    makeObservable(this, {
      processing: observable,
      loaded: observable,
      results: observable,
      pagination: observable,

      // actions
      reset: action,
      load: action,
      remove: action,
      setPage: action,
    });
  }

  get pages() {
    return (
      Math.floor(this.pagination.count / ITEMS_ON_PAGE) +
      (this.pagination.count % ITEMS_ON_PAGE ? 1 : 0)
    );
  }

  get page() {
    return this.pagination.offset / ITEMS_ON_PAGE + 1;
  }

  reset = () => {
    this.loaded = false;
    this.processing = false;
    this.results = [];
    this.pagination = defaultPagination();
  };

  load = () => {
    this.processing = true;
    this.api.list
      .get({
        pagination: this.pagination,
      })
      .then(({ data }) => {
        this.loaded = true;
        this.processing = false;
        this.results = data.results;
        this.pagination.count = data.count;
      })
      .catch(() => {
        this.processing = false;
        this.loaded = false;
      });
  };

  remove = (id) => {
    this.api.detail.delete(id).then(this.load);
  };

  setPage = (page) => {
    console.log(page);
    this.pagination.offset = ITEMS_ON_PAGE * (page - 1);
    this.load();
  };
}

export default AsyncList;
