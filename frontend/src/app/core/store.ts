import BookListModel from 'app/components/books/book-list/BookListModel';
import BookCreateModel from 'app/components/books/book-create/BookCreateModel';
import BookDetailModel from 'app/components/books/book-detail/BookDetailModel';
import AuthorListModel from 'app/components/authors/author-list/AuthorListModel';
import AuthorCreateModel from 'app/components/authors/author-create/AuthorCreateModel';
import AuthorDetailModel from 'app/components/authors/author-detail/AuthorDetailModel';
import ShelfListModel from 'app/components/shelves/shelf-list/ShelfListModel';
import ShelfDetailModel from 'app/components/shelves/shelf-detail/ShelfDetailModel';
import ShelfCreateModel from 'app/components/shelves/shelf-create/ShelfCreateModel';

export class Store {
  bookList: BookListModel;
  bookCreate: BookCreateModel;
  bookDetail: BookDetailModel;

  authorList: AuthorListModel;
  authorCreate: AuthorCreateModel;
  authorDetail: AuthorDetailModel;

  shelfList: ShelfListModel;
  shelfDetail: ShelfDetailModel;
  shelfCreate: ShelfCreateModel;

  constructor() {
    this.bookList = new BookListModel();
    this.bookCreate = new BookCreateModel();
    this.bookDetail = new BookDetailModel();

    this.authorList = new AuthorListModel();
    this.authorCreate = new AuthorCreateModel();
    this.authorDetail = new AuthorDetailModel();

    this.shelfList = new ShelfListModel();
    this.shelfDetail = new ShelfDetailModel();
    this.shelfCreate = new ShelfCreateModel();
  }
}

export interface InjectStore {
  store: Store;
}

const store = new Store();

export default store;
