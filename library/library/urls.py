from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from rest_framework import routers, serializers, viewsets
from books.api import (
    BookViewSet,
    AuthorViewSet,
    ShelfViewSet,
)


router = routers.DefaultRouter()
router.register(r'books', BookViewSet)
router.register(r'authors', AuthorViewSet)
router.register(r'shelves', ShelfViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/', include(router.urls)),
]
