from django.urls import path, include
from rest_framework import routers,  viewsets, generics, pagination
from .models import Book, Author, Shelf
from .serializeers import BookSerializer, AuthorSerializer, ShelfSerializer


class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer

    def get_queryset(self):
        queryset = Book.objects
        author_id = self.request.query_params.get('author_id')
        shelf_id = self.request.query_params.get('shelf_id')

        if shelf_id:
            queryset = queryset.filter(shelves__id=shelf_id)

        if author_id:
            queryset = queryset.filter(authors__id=author_id)

        queryset = queryset.order_by('order')

        return queryset.all()


class AuthorViewSet(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer

    def get_queryset(self):
        queryset = Author.objects
        name = self.request.query_params.get('name')

        if name:
            result = queryset.filter(name__contains=name)
            return result

        return queryset.all()


class ShelfViewSet(viewsets.ModelViewSet):
    queryset = Shelf.objects.all()
    serializer_class = ShelfSerializer


router = routers.DefaultRouter()
router.register(r'books', BookViewSet)
router.register(r'authors', AuthorViewSet)
router.register(r'shelves', ShelfViewSet)


urlpatterns = [
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]
