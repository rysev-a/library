from rest_framework import serializers, validators
from django.db.models import Max


from .models import Book, Author, Shelf


class GetInfoSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField(
        max_length=200, allow_blank=True, required=False)


class GetOrderSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    order = serializers.IntegerField()
    shelf = GetInfoSerializer()


class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = [
            'id',
            'title',
            'description',
            'authors',
            'shelves',
            'order'
        ]

    title = serializers.CharField(max_length=200,
                                  validators=[validators.UniqueValidator(
                                      queryset=Book.objects.all())]
                                  )
    description = serializers.CharField(max_length=200)
    authors = GetInfoSerializer(many=True)
    shelves = GetInfoSerializer(many=True)

    def create(self, validated_data):

        book = Book.objects.create(
            title=validated_data.get('title'),
            description=validated_data.get('description'),
            order=Book.objects.aggregate(Max('order')).get('order__max')+1
        )

        for author_data in validated_data.get('authors'):
            author_id = author_data.get('id')
            book.authors.add(author_id)

        for shelf_data in validated_data.get('shelves'):
            shelf_id = shelf_data.get('id')
            book.shelves.add(shelf_id)

        return book

    def update(self, book, data):
        book.order = data.get('order')
        book.save()

        return book


class ShelfSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Shelf
        fields = ['id', 'name']

    name = serializers.CharField(max_length=200,
                                 validators=[validators.UniqueValidator(
                                     queryset=Shelf.objects.all())]
                                 )


class AuthorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Author
        fields = ['id', 'name']

    name = serializers.CharField(max_length=200,
                                 validators=[validators.UniqueValidator(
                                     queryset=Author.objects.all())]
                                 )
