from django.db import models


class Author(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Shelf(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Book(models.Model):
    title = models.CharField(max_length=200, unique=True)
    description = models.CharField(max_length=200)

    authors = models.ManyToManyField(Author)
    shelves = models.ManyToManyField(Shelf)
    order = models.IntegerField()

    def __str__(self):
        return self.title
