# Сервис библиотека ([демо-ссылка](http://library.fvds.ru))

## Основные возможности:

1. Дбавление книги
2. Добавление автора
3. Добавление полки
4. Просмотр книг автора
5. Просмотр книг на полке и их сортировка

## Используемые технологии

### Серверная часть

Серверная часть написана с помощью фреймворка django,
сервера nginx и базы данных postgresql  
 В серверной части используются следующие основные библиотеки

- django - основной фреймворк приложения
- django-rest-framework - создание api приложения

---

### Клиентская часть

Клиентская часть собирается с помощью сборщика webpack,
библиотеки для клиентской части

- react - библиотека для отрисовки DOM-элементов
- react-select - библиотека для работы с селектом
- react-sortable-hoc - библиотека для сортировки элементов
- mobx - библиотека для управления состоянием приложения
